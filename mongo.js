var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var fs = require('fs');
const winston = require('winston');
winston.level = 'info';


const mongoUrl = "mongodb://admin:YGTNPAZOJMGEVLQX@sl-eu-lon-2-portal.3.dblayer.com:15827/mydb?authSource=admin";


var db;
var options = {
    mongos: {
        ssl: true,
        sslValidate: false,
    }
}

MongoClient.connect(mongoUrl, options, function(err, mongoDB) {
    assert.equal(null, err);
    db = mongoDB;
    winston.log('info', "Connected to mongodb");

});

module.exports = {
    listCollections: function () {

        db.listCollections({}).toArray(function(err, collections) {
            assert.equal(null, err);
            collections.forEach(function(collection) {
                console.log(collection);
            });
        });
    },
    insertVenue: function(venue, callback){
        venue._id = venue.locationId;
        db.collection('venues').update({_id: venue._id}, {$set: venue}, {upsert: true}, function(err, result){
            assert.equal(err, null);
            if(err == null)
                callback(true);
            else
                callback(false);
        });
    },
    getVenues: function(callback) {
        db.collection('venues').find({}, {_id: 0}, {"locationId": 1, "locationName": 1}).toArray(function (err, result) {
            if (err) {
                console.log(err);
                callback(null);
            } else if (result.length) {
                callback(result);
            } else {
                winston.log('info', 'No document(s) found with defined "find" criteria!');
                callback(null);
            }
        });
    },
    getVenue: function(id, callback) {
        db.collection('venues').findOne({"locationId": id}, {_id: 0}, function(err, document){
            if (err) {
                console.log(err);
                callback(null);
            } else if (document) {
                callback(document);
            } else {
                winston.log('info', 'No document(s) found with defined "find" criteria!');
                callback(null);
            }
        });
    },
    getAllVenuesData: function(callback) {
        db.collection('venues').find({}, {_id: 0}).toArray(function (err, result) {
            if (err) {
                console.log(err);
                callback(null);
            } else if (result.length) {
                callback(result);
            } else {
                winston.log('info', 'No document(s) found with defined "find" criteria!');
                callback(null);
            }
        });
    },
    userLikesVenue: function(userId, venueId, callback) {
        db.collection('users').update({"userId": userId}, {$addToSet: {likes: venueId}}, {upsert: true}, function(err, result){
            assert.equal(err, null);
            if(err == null)
                callback(true);
            else
                callback(false);
        });
    },
    getUserLikes: function(userId, callback) {
        db.collection('users').findOne({"userId": userId}, function (err, document) {
            if (err) {
                console.log(err);
                callback(null);
            } else if (document) {
                callback(document.likes);
            } else {
                winston.log('info', 'No document(s) found with defined "find" criteria!');
                callback([]);
            }
        });
    },
    removeUserLike: function(userId, venueId, callback) {
        db.collection('users').update({"userId": userId}, {$pull: {likes: venueId}}, function (err, document) {
            if (err) {
                console.log(err);
                callback(false);
            } else if (document) {
                callback(true);
            } else {
                callback(true);
            }
        });
    }
}

