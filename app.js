var express = require('express');
var path = require('path');
var app = express();
var bodyParser = require('body-parser');
var fs = require('fs');
var request = require('request');
var queryString = require('query-string');
const winston = require('winston');
winston.level = 'info';

const mecenasUrl = "http://127.0.0.1:8080/";

winston.add(winston.transports.File, {level: 'info', filename: 'info.log'})


var mongo = require('./mongo');

app.set('view engine', 'pug');
app.set('views', path.join(__dirname, '/views'));
app.use(express.static('views/res'));

app.use(bodyParser.json())


app.get('/', function (req, res) {
	mongo.listCollections();
	res.render('index', { title: 'Hey', message: 'Hello there!' });
});

app.get('/createVenue/', function (req, res) {	
	res.render('createVenue', { title: 'Hey', message: 'Hello there!' });
});

app.post('/uploadVenue/', function(req, res){
	// writeFile(JSON.parse(req.body), function(success){
	mongo.insertVenue(req.body, function(success){
		if(success)
            winston.log('info', "Inserted venue " + req.body.locationName + " with success");

	});
	writeFile(req.body, function(success){
		if(success)
			res.status(200).send('');
		else
			res.status(300).send('');
	});
});
app.get('/listVenues/', function (req, res) {	
	var dirname = path.join(__dirname, 'venueData');
	mongo.getVenues(function(result){
		res.render('listVenues', {"venues":result});

	})
	
});
app.get('/editVenue/', function (req, res) {
	mongo.getVenue(req.query.venue, function(venue){
		res.render('editVenue', {"venue": venue});
	});
});






// JSON requests

var venuesGlobal = [];
app.get('/app/getAllVenues/', function(req, res){
	getAllVenues(function(){
        res.send(JSON.stringify(venuesGlobal));
    });
});

var getAllVenues = function(callback){
    mongo.getVenues(function(venues){
        if(venues != undefined && venues.length != 0)
            venuesGlobal = venues;
        callback();
    });
}

app.get('/app/getVenueTrendReport/:id', function(req, res){
	if(venuesGlobal.length == 0){
		getAllVenues(function(){
			if(venuesGlobal.length == 0)
				res.status(500).send();
			var myResponse = {};
			venuesGlobal.forEach(function(listItem, i){
                if(venuesGlobal[i].locationId == req.params.id){
                    myResponse = venuesGlobal[i];
                    var requestsPending = 0;
                    var requestDone = function(){
                        requestsPending --;
                        if(requestsPending <= 0)
                            res.status(200).send(myResponse);
                    }
                    if(myResponse.exhibitions != undefined){
                    	myResponse.exhibitions.forEach(function(listItem, j){
                            if(myResponse.exhibitions[j].artists != undefined){
                                myResponse.exhibitions[j].artists.forEach(function(listItem, w){
                                    requestsPending ++;
                                    var query = {};
                                    query.keyword = myResponse.exhibitions[j].artists[w].name;
                                    var urlS = mecenasUrl + "trend?" + queryString.stringify(query);
                                    request.get(urlS, function (error, resP, body) {
                                        if (!error && resP.statusCode == 200) {
                                            myResponse.exhibitions[j].artists[w].report = body;
                                            requestDone();
                                        }
                                        else{
                                            res.status(500).send();
                                        }
                                    });
								});
                            }
						});
                    }
                }
			});

		});
	}
});

app.get("/app/userLikes/:venueId", function(req, res){
	console.log("added like to " + req.params.venueId);
	mongo.userLikesVenue(1, req.params.venueId, function(success){
		if(success)
			res.status(200).send();
		else
			res.status(500).send();
	});
});
app.get("/app/getUserLikes/", function(req, res){
	mongo.getUserLikes(1, function(likes){
		if(likes)
			res.status(200).send(likes);
		else
			res.status(500).send();
	});
});
app.get("/app/removeUserLike/:venueId", function(req, res){
    mongo.removeUserLike(1, req.params.venueId, function(success){
        if(success)
            res.status(200).send();
        else
            res.status(500).send();
    });
});


var port = (process.env.VCAP_APP_PORT || 3002);
var host = (process.env.VCAP_APP_HOST || 'localhost');

app.listen(port, function () {
  console.log('Example app listening on ' + host + ':' + port);
})



var writeFile = function(data, callback){
	fs.writeFile(path.join(__dirname, "venueData", data.locationId + ".txt"), JSON.stringify(data), function(err) {
	    if(err) {
			console.log("Error in writing file");
	        callback(false);
	    }
	    callback(true);
	});
}

