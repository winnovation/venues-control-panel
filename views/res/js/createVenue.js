var newExhibitionEvent = function(jqueryObject){
	var newExhibition = $("<div></div>");
	newExhibition.addClass("exhibition-row");

	var nameForm = $('<div class="form-group"><label>Exhibition name</label><input class="form-control" type="text" name="exhibitionName" placeholder="Insert exhibition name">');
	var begginingDateForm = $('<div class="form-group"><label>Beggining date (Insert 0 if it\'s a permanent exhibition)</label><div class="input-group"><div class="input-group-addon"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></div><input class="form-control datepicker" type="text" name="exhibitionBegDate" placeholder="Insert beggining date"></div></div>');
	var endDateForm = $('<div class="form-group">' +
							'<label>Ending date (Insert 0 if it\'s a permanent exhibition)</label>' +
							'<div class="input-group">' +
								'<div class="input-group-addon">' +
									'<span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>' +
								'</div>' +
								'<input class="form-control datepicker" type="text" name="exhibitionEndDate" placeholder="Insert ending date">' +
							'</div>' +
						'</div>');
	var addArtist = $('<div class="artist-row"><button class="btn btn-primary btn-sm artistButton" type="button">Add Artist</button></div>');

	newExhibition.append(nameForm);
	newExhibition.append(begginingDateForm);
	newExhibition.append(endDateForm);
	newExhibition.append(addArtist);

	newExhibition.insertBefore(jqueryObject.parent().parent().children().last());

}

var newArtistEvent = function(jqueryObject) {
	var artistsRow = $('<div></div>');
	artistsRow.addClass("artist-row");
	var inputLine = $('<div class="form-group"><label>Artist name</label><input class="form-control" type="text" name="artistName" placeholder="Insert artist name"></div>');

	artistsRow.append(inputLine);

	var artPieceRow = $('<div></div>');
	artPieceRow.addClass("art-piece-row");
	artPieceRow.addClass("form-inline");
	var artPieceNameForm = $('<div class="form-group"><input type="text" name="artPieceName" placeholder="Insert name of piece"></div>');
	var artPieceDateForm = $('<div class="form-group">' +
								'<div class="input-group">' +
									'<input type="text" name="artPieceYear" placeholder="Insert year of piece">' +
									'<div class="input-group-addon">' +
										'<span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>' +
									'</div>' +
								'</div>' +
							'</div>');
	artPieceRow.append(artPieceNameForm);
	artPieceRow.append(artPieceDateForm);
	
	artistsRow.append(artPieceRow);

	var addArtPiece = $('<div class="art-piece-row"></div>');
	var addArtPieceButton = $('<button class="button btn btn-primary btn-sm artPieceButton" type="button">Add Art Piece</button>')
	addArtPiece.append(addArtPieceButton);

	artistsRow.append(addArtPiece);


	artistsRow.insertBefore(jqueryObject.parent().parent().children().last());

	// jqueryObject.parent().parent().prepend(artistsRow);
}

var newPieceEvent = function(jqueryObject) {
	console.log("ola");
	var artPieceRow = $('<div></div>');
	artPieceRow.addClass("art-piece-row");
	artPieceRow.addClass("form-inline");
	var artPieceNameForm = $('<div class="form-group"><input type="text" name="artPieceName" placeholder="Insert name of piece"></div>');
	var artPieceDateForm = $('<div class="form-group">' +
		'<div class="input-group">' +
		'<input type="text" name="artPieceYear" placeholder="Insert year of piece">' +
		'<div class="input-group-addon">' +
		'<span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>' +
		'</div>' +
		'</div>' +
		'</div>');
	artPieceRow.append(artPieceNameForm);
	artPieceRow.append(artPieceDateForm);

	var rootParent = jqueryObject.parent().parent();
	artPieceRow.insertBefore(rootParent.children().last());


}


var sendToServer = function(){
	if(locationId == ""){
		alert("You have to select a venue in the map");
		$("html, body").animate({ scrollTop: 0 }, "slow");
		$("#pac-input").focus();
	}
	else{
		var objectToSend = {};
		objectToSend.locationId = locationId;
		objectToSend.locationName = locationName;
		objectToSend.updatedOn = new Date().getTime();
		objectToSend.exhibitions = [];

		var exhibitionsJquery = $(".exhibition-row");
		for(i = 0; i < exhibitionsJquery.length - 1; i++){
			var exhibition = {};
			exhibition.name = exhibitionsJquery.eq(i).find("[name='exhibitionName']").val();
			exhibition.begDate = exhibitionsJquery.eq(i).find("[name='exhibitionBegDate']").val();
			exhibition.endDate = exhibitionsJquery.eq(i).find("[name='exhibitionEndDate']").val();
			
			exhibition.artists = [];

			var artistsJquery = exhibitionsJquery.eq(i).find(".artist-row");
			for(j = 0; j < artistsJquery.length - 1; j++){
				var artist = {};
				artist.name = artistsJquery.eq(j).find("[name='artistName']").val();
				artist.pieces = [];

				var artPiecesJquery = artistsJquery.eq(j).find(".art-piece-row");
				for(z = 0; z < artPiecesJquery.length - 1; z++){
					var piece = {};
					piece.name = artPiecesJquery.eq(z).find("[name='artPieceName']").val();
					piece.year = artPiecesJquery.eq(z).find("[name='artPieceYear']").val();

					artist.pieces.push(piece);

				}
				exhibition.artists.push(artist);
			}


			objectToSend.exhibitions.push(exhibition);
		}
		$.post({
			"url": "uploadVenue",
			"contentType": "application/json",
			error: function(){
				alert(error);
			},
			success: function(){
				alert("success!");
				window.location.replace("/listVenues");
			},
			"data": JSON.stringify(objectToSend)
		});
	}
	
}

$(document).ready(function(){
	$("#exhibitions").on('click', '.artistButton', function(){
		newArtistEvent($(this));
		$(".datepicker").datepicker();
	});	

	$("#exhibitions").on('click', '.artPieceButton', function(){
		newPieceEvent($(this));
		$(".datepicker").datepicker();
	});	

	$("#exhibitions").on('click', '.exhibitionButton', function(){
		newExhibitionEvent($(this));
		$(".datepicker").datepicker();
	});	
	$("#submitButton").click(function(){
		sendToServer(function(){
		});
	});
	$(".datepicker").datepicker();
});
