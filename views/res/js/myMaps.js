          // This sample uses the Place Autocomplete widget to allow the user to search
          // for and select a place. The sample then displays an info window containing
          // the place ID and other information about the place that the user has
          // selected.

          // This example requires the Places library. Include the libraries=places
          // parameter when you first load the API. For example:
          // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
    // var locationId = "";
    var locationName = "";
    var locationId = "";
    //var marker;

    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 52.377965, lng: 4.9002023},
          zoom: 13
        });

        var input = document.getElementById('pac-input');

        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
          map: map
        });
        marker.addListener('click', function() {
          infowindow.open(map, marker);
        });
        if(locationId != ""){
            var request = {
                placeId: locationId
            };

            var service = new google.maps.places.PlacesService(map);
            service.getDetails(request, callback);

            // Checks that the PlacesServiceStatus is OK, and adds a marker
            // using the place ID and location from the PlacesService.
            function callback(result, status) {
                if (status == google.maps.places.PlacesServiceStatus.OK) {
                    marker.setPlace({
                        placeId: result.place_id,
                        location: result.geometry.location
                    });
                    marker.setVisible(true);

                    locationId = result.place_id;
                    locationName = result.name;

                    infowindow.setContent('<div><strong>This is the venue<br>' + result.name + '</strong><br>' +
                        result.formatted_address);
                    infowindow.open(map, marker);
                }
            }
        }

        // marker.setPlace({placeId: locationId});


        autocomplete.addListener('place_changed', function() {
            infowindow.close();
            var place = autocomplete.getPlace();
            if (!place.geometry) {
              return;
            }

            if (place.geometry.viewport) {
              map.fitBounds(place.geometry.viewport);
            } else {
              map.setCenter(place.geometry.location);
              map.setZoom(17);
            }

            // Set the position of the marker using the place ID and location.
            marker.setPlace({
              placeId: place.place_id,
              location: place.geometry.location
            });
            marker.setVisible(true);

            locationId = place.place_id;
            locationName = place.name;

            infowindow.setContent('<div><strong>This is the venue<br>' + place.name + '</strong><br>' +
                place.formatted_address);
            infowindow.open(map, marker);
        });
    }